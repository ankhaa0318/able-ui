import { ComponentType, ReactNode } from 'react';
import { ViewProps } from 'react-native';
declare type ViewPropsWithoutStyle = Omit<ViewProps, 'style'>;
export interface RenderCellOptions {
    symbol: string;
    index: number;
    isFocused: boolean;
}
export interface Props extends ViewPropsWithoutStyle {
    RootProps?: ViewProps;
    rootStyle?: ViewProps['style'];
    RootComponent?: ComponentType<ViewProps>;
    renderCell: (options: RenderCellOptions) => ReactNode;
}
export declare const LoginLayout: ({ rootStyle }: Props) => JSX.Element;
export {};
