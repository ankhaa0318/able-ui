import { StyleProp, ViewStyle } from 'react-native';
export declare const getStyle: (base: StyleProp<ViewStyle>, custom?: StyleProp<ViewStyle>) => StyleProp<ViewStyle>;
