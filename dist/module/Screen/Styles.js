import { StyleSheet } from 'react-native';
const input = {
  width: '80%',
  height: 50,
  paddingLeft: 12,
  borderColor: '#a3a0a0',
  borderBottomWidth: 2,
  color: '#373737',
  alignSelf: 'center',
  fontWeight: 'bold'
};
const phoneInput = {
  marginTop: 60,
  marginBottom: '0.08%'
};
const passwordInput = {
  marginTop: 10,
  alignSelf: 'center'
};
const noValidInput = {
  backgroundColor: '#FCC7C7',
  borderColor: '#CC8A8A'
};
export default StyleSheet.create({
  logo: {
    marginTop: 90,
    width: 100,
    height: 100,
    alignSelf: 'center',
    marginBottom: 50
  },
  userName: { ...input,
    ...phoneInput
  },
  phoneInputNoValid: { ...input,
    ...phoneInput,
    ...noValidInput
  },
  passwordInput: Object.assign({}, input, passwordInput),
  passwordInputNoValid: Object.assign({}, input, passwordInput, noValidInput),
  scrollContainer: {
    flex: 1,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'white'
  },
  mainbutton: {
    width: '80%',
    height: 50,
    backgroundColor: '#dd6365',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 40,
    borderRadius: 10
  },
  mainbuttontext: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  subbutton: {
    width: 169,
    height: 50,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 30,
    borderRadius: 28,
    color: '#a7a6a6'
  },
  subbuttontext: {
    alignSelf: 'center',
    color: 'gray',
    fontSize: 16,
    fontWeight: 'bold'
  },
  checktext: {
    alignSelf: 'center',
    color: 'gray',
    fontSize: 14,
    fontWeight: 'bold'
  },
  checkContainer: {
    marginTop: 20,
    width: '80%',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  checkBoxContainer: {
    marginTop: 10,
    width: '80%',
    flexDirection: 'row',
    alignSelf: 'center'
  },
  checkStyle: {
    marginRight: 5,
    alignSelf: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'red'
  }
});
//# sourceMappingURL=Styles.js.map