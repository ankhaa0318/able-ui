import React from 'react';
import { View } from 'react-native';
import { getStyle } from './utils';

const styles = require('./Styles');

export const LoginLayout = ({
  rootStyle
}) => {
  return /*#__PURE__*/React.createElement(View, {
    style: getStyle(styles.container, rootStyle)
  });
};
//# sourceMappingURL=index.js.map