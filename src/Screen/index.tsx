import React,{ComponentType,ReactNode} from 'react';
import {View,ViewProps} from 'react-native';
import {getStyle} from './utils';

type ViewPropsWithoutStyle = Omit<ViewProps, 'style'>;

export interface RenderCellOptions {
  symbol: string;
  index: number;
  isFocused: boolean;
}

const styles = require('./Styles')
export interface Props extends ViewPropsWithoutStyle {
  RootProps?: ViewProps;
  rootStyle?: ViewProps['style'];
  RootComponent?: ComponentType<ViewProps>;
  renderCell: (options: RenderCellOptions) => ReactNode;
}
export const LoginLayout = ({rootStyle}: Props)=>{
  return (
    <View style={getStyle(styles.container, rootStyle)}/>
  )
}
