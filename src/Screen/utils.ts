import {StyleProp, ViewStyle} from 'react-native';

export const getStyle = (
  base: StyleProp<ViewStyle>,
  custom?: StyleProp<ViewStyle>,
) => (custom ? [base, custom] : base);