/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {LoginScreen} from 'Able_ui';

const App = () => {
  return (
    <>
      <LoginScreen rootStyle={{backgroundColor: 'red'}} />
    </>
  );
};

export default App;
